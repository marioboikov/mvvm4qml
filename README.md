# mvvm4qml

## Share & Discuss
If you are interested in the MVVM topic for QML, do not hesitate to share and discuss together with us. You
can even colloborate!

## Wiki
For more information on the topic, you can have a look at our [wiki](https://bitbucket.org/marioboikov/mvvm4qml/wiki).