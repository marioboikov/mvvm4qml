# MVVMLoginExamples
The examples shows variants of Sam Verschueren's MVVMLoginExample which you can
find in [gitrepo]/SamVerschueren/BB10-MVVMLoginExample/.

## MVVMLoginExample1
This example shows that it might not always be a good idea to practice MVVM
in a QML application. Some things are just more easy to achieve by being pragmatic. The application is more of a traditional MVC app where *main.qml* is acting the *controller* and sets up the view and connects the logic to the view.

## MVVMLoginExample2
This example shows that you don't have to do a *code behind* class for a view model. Just because that's what you do in XAML/C#  doesn't mean it has to be done the same way with QML. Instead, the view model is implemented as yet another QML file which pretty much does the same as the LoginViewModel in Sam's example.

