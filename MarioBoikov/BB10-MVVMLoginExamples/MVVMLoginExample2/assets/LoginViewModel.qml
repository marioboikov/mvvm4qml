import QtQuick 1.0
import org.beblue 1.0

QtObject {
    id: viewModel

    property string username
    property string password

    signal loginFinished(bool loggedIn)

    function login() {
        loginService.login(username, password)
    }

    property variant loginService: LoginService {
        onLoginFinished: {
            viewModel.loginFinished(status === LoginService.Accepted)
        }
    }
}
