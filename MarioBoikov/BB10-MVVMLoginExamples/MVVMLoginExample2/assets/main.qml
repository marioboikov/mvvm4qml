import bb.cascades 1.0
import bb.system 1.0

Page {    
    titleBar: TitleBar {
        title: qsTr("MVVM Login Example2")
    }

    Container {
        topPadding: 50
        leftPadding: 10
        rightPadding: 10

        layout: StackLayout {
        }

        TextField {
            id: txtUsername
            text: loginVM.username
            hintText: qsTr("Username")
        }

        TextField {
            id: txtPassword
            text: loginVM.password
            hintText: qsTr("Password")
            inputMode: TextFieldInputMode.Password
        }

        Button {
            text: qsTr("Login")
            horizontalAlignment: HorizontalAlignment.Center
            imageSource: "asset:///images/lock.png"
            onClicked: loginVM.login()
        }
    }

    attachedObjects: [
        LoginViewModel {
            id: loginVM

            username: txtUsername.text
            password: txtPassword.text

            onLoginFinished: {
                if(loggedIn) {
                    dialog.body = "Successfully logged in";
                } else {
                    dialog.body = "Wrong credentials, try again";
                }
                
                dialog.show()
            }
        },

        SystemDialog {
            id: dialog
            cancelButton.label: undefined
            title: qsTr("Login")
        }
    ]
}
