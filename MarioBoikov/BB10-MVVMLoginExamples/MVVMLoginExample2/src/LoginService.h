/*
 * LoginService.h
 *
 *  Created on: 12 feb 2013
 *      Author: mario
 */

#ifndef LOGINSERVICE_H_
#define LOGINSERVICE_H_

#include <QObject>

class LoginService : public QObject
{
    Q_OBJECT
    Q_ENUMS(Status)

public:
    enum Status {
        Accepted,
        Rejected
    };

    LoginService(QObject *parent=0);

    Q_INVOKABLE void login(const QString& username, const QString& password);

signals:
    void loginFinished(Status status);

};

#endif /* LOGINSERVICE_H_ */
