// Default empty project template
#include "MVVMLoginExample1.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>

#include "LoginService.h"

using namespace bb::cascades;

MVVMLoginExample1::MVVMLoginExample1(bb::cascades::Application *app)
: QObject(app)
{
    qmlRegisterType<LoginService>("org.beblue", 1, 0, "LoginService");

    // create scene document from main.qml asset
    // set parent to created document to ensure it exists for the whole application lifetime
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    // create root object for the UI
    AbstractPane *root = qml->createRootObject<AbstractPane>();
    // set created root object as a scene
    app->setScene(root);
}

