/*
 * LoginService.cpp
 *
 *  Created on: 12 feb 2013
 *      Author: mario
 */

#include "LoginService.h"

LoginService::LoginService(QObject *parent) : QObject(parent)
{
}

void LoginService::login(const QString& username, const QString& password)
{
    Status status = username == "Sam" && password == "test" ? Accepted : Rejected;
    emit loginFinished(status);
}

