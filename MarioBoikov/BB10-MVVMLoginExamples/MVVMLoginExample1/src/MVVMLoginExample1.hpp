// Default empty project template
#ifndef MVVMLoginExample1_HPP_
#define MVVMLoginExample1_HPP_

#include <QObject>

namespace bb { namespace cascades { class Application; }}

/*!
 * @brief Application pane object
 *
 *Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */
class MVVMLoginExample1 : public QObject
{
    Q_OBJECT
public:
    MVVMLoginExample1(bb::cascades::Application *app);
    virtual ~MVVMLoginExample1() {}
};


#endif /* MVVMLoginExample1_HPP_ */
