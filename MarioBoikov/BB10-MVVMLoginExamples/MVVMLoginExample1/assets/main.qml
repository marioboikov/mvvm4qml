import bb.cascades 1.0
import bb.system 1.0
import org.beblue 1.0

Page {    
    titleBar: TitleBar {
        title: qsTr("MVVM Login Example1")
    }

    Container {
        topPadding: 50
        leftPadding: 10
        rightPadding: 10

        layout: StackLayout {
        }

        TextField {
            id: txtUsername
            hintText: qsTr("Username")
        }

        TextField {
            id: txtPassword
            hintText: qsTr("Password")
            inputMode: TextFieldInputMode.Password
        }

        Button {
            text: qsTr("Login")
            horizontalAlignment: HorizontalAlignment.Center
            imageSource: "asset:///images/lock.png"
            onClicked: loginService.login(txtUsername.text, txtPassword.text)
        }
    }

    attachedObjects: [
        LoginService {
            id: loginService

            onLoginFinished: {
                if(status === LoginService.Accepted) {
                    dialog.body = "Successfully logged in";
                }
                else {
                    dialog.body = "Wrong credentials, try again";
                }
                
                dialog.show()
            }
        },

        SystemDialog {
            id: dialog
            cancelButton.label: undefined
            title: qsTr("Login")
        }
    ]
}
