import bb.cascades 1.0

Page {
    titleBar: TitleBar {
        title: qsTr("ToolTipCalculator")
    }

    Container {
        topPadding: 20.0
        rightPadding: 20.0
        bottomPadding: 20.0
        leftPadding: 20.0
        layout: StackLayout {
        }

        TextField {
            layoutProperties: StackLayoutProperties {
                spaceQuota: 3
            }
            inputMode: TextFieldInputMode.NumbersAndPunctuation
            hintText: qsTr("Bill Total")
        }

        Container {
            topPadding: 60
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: "10%"
                textStyle.textAlign: TextAlign.Center
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: "15%"
                textStyle.textAlign: TextAlign.Center
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: "20%"
                textStyle.textAlign: TextAlign.Center
            }
        }

        Container {
            topPadding: 30
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: qsTr("Tip")
                verticalAlignment: VerticalAlignment.Center
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: "0.0"
                textStyle.textAlign: TextAlign.Center
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: "0.0"
                textStyle.textAlign: TextAlign.Center
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: "0.0"
                textStyle.textAlign: TextAlign.Center
            }
        }

        Container {
            topPadding: 30
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: qsTr("Total")
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: "0.0"
                textStyle.textAlign: TextAlign.Center
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: "0.0"
                textStyle.textAlign: TextAlign.Center
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: "0.0"
                textStyle.textAlign: TextAlign.Center
            }
        }

        Container {
            topPadding: 30
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: qsTr("Custom")
            }

            Slider {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 2
                }
            }

            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                text: "0%"
                textStyle.textAlign: TextAlign.Center
            }
        }
    }
}
