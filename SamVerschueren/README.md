# Sam Verschueren

## Who am I?
I'm a student developer who's interested in mobile applications. Recently I discovered the BlackBerry 10
platform, and I really like their approach. I #BB10 Believe, that's for sure! I want to extend my own knowledge
and that of other developers.

## mvvm4qml
In a quest of searching for some good principles regarding separation of concerns on the BlackBerry 10 platform, I
came across a tweet of @marioboikov indicating that he allready researched the MVVM design principle on BB10. I
tweeted him and asked if he had documented his research but he didn't. That's why we came up with the idea of
creating a spot on the web where everybody can collaborate on the MVVM topic.

## github
https://github.com/SamVerschueren