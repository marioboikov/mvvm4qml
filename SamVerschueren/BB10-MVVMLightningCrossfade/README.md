# LightningCrossfade

The LightningCrossfade app is the first application that you build when you start reading the documentation
of BlackBerry. The application can be found at the (Getting Started)[http://developer.blackberry.com/cascades/documentation/getting_started/first_app/index.html] section
of the website.

## What's the difference?
The difference with the normal example on the BlackBerry developers website is that I used a ViewModel to keep track of
the value of the slider. Actually using MVVM for this is just overkill. For such a tiny project, I would not recommand 
using MVVM. It's not worth it writing a ViewModel for this.

## Why MVVM for this?
As said before, I would not recommand someone to use a ViewModel for this. The getting started example of BlackBerry is
good enough with much more less code then my example. I want to show you how the binding with the ViewModel is done. 
It's easier to see what happens and how things are done on a tiny example then on a huge project, especially for beginners
in programming or even beginners in MVVM.