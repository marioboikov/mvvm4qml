/*
 * LightningViewModel.hpp
 *
 * The header file of the ViewModel.
 *
 * Created on: 15-feb.-2013
 * Author: Sam Verschueren        <sam.verschueren@gmail.com>
 */

#ifndef LIGHTNINGVIEWMODEL_HPP_
#define LIGHTNINGVIEWMODEL_HPP_

#include <QObject>

class LightningViewModel : public QObject {
    Q_OBJECT

    Q_PROPERTY(float dayNightRatio READ getDayNightRatio WRITE setDayNightRatio NOTIFY dayNightRatioChanged FINAL)

    Q_SIGNALS:
        void dayNightRatioChanged(float ratio);

    private:
        float dayNightRatio;

    public:
        LightningViewModel(QObject *parent=0) : QObject(parent) { };

        float getDayNightRatio() const;
        void setDayNightRatio(float ratio);
};

#endif /* LIGHTNINGVIEWMODEL_HPP_ */
