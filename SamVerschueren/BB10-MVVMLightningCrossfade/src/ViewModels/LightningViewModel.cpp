/*
 * LightningViewModel.cpp
 *
 * The source file of the ViewModel. Everything is implemented over here.
 *
 * Created on: 15-feb.-2013
 * Author: Sam Verschueren        <sam.verschueren@gmail.com>
 */

#include "LightningViewModel.hpp"

/**
 * This method returns the DayNightRatio. It's a constant method
 * because it does nog change anything.
 *
 * @return float Returns the ratio.
 */
float LightningViewModel::getDayNightRatio() const {
    return this->dayNightRatio;
}

/**
 * This method sets the DayNightRatio to the ratio provided in the
 * parameter. If the ratio is not the same is the previous ratio, the
 * ratio gets updated and the dayNightRatioChanged signal is emitted.
 *
 * @param ratio The ratio that is provided by (in this case) the slider.
 */
void LightningViewModel::setDayNightRatio(float ratio) {
    if(this->dayNightRatio != ratio) {
        this->dayNightRatio = ratio;

        emit dayNightRatioChanged(ratio);
    }
}
