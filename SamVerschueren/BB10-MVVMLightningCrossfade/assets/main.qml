import bb.cascades 1.0
import mvvm.viewmodels 1.0

Page {
    Container {
        id: containerID
        topPadding: 20.0
        background: Color.create("#262626")
        layout: StackLayout { }

        // The container that holds the 2 images
        Container {
            id: imageContainer
            horizontalAlignment: HorizontalAlignment.Center
            layout: DockLayout {
            }

            ImageView {
                id: night
                imageSource: "asset:///images/night.jpg"
                horizontalAlignment: HorizontalAlignment.Center
            }

            /**
             * Binds the opacity of this image to the ratio of the
             * LightningViewModel
             */
            ImageView {
                id: day
                opacity: lightningVM.dayNightRatio
                imageSource: "asset:///images/day.jpg"
                horizontalAlignment: HorizontalAlignment.Center
            }
        }

        Container {
            leftPadding: 20
            rightPadding: 20
            topPadding: 25
            bottomPadding: 25
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            ImageView {
                imageSource: "asset:///images/moon.png"
                verticalAlignment: VerticalAlignment.Center
            }

            /**
             * Instead of listening to the onImmediateValueChanged signal,
             * we do nothing over here. The value of this slider is binded
             * to the ViewModel at the bottom in the attachedObjects.
             */
            Slider {
                id: slider
                leftMargin: 20
                rightMargin: 20
                horizontalAlignment: HorizontalAlignment.Fill
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }

            ImageView {
                imageSource: "asset:///images/sun.png"
                verticalAlignment: VerticalAlignment.Center
            }
        }
    }

    attachedObjects: [
        /** 
         * The dayNightRatio property is bound to the immediateValue of
         * the slider. When the value is changed, the onDayNightRatioChanged
         * signal is emitted and the opacity of the image is updated.
         */
        LightningViewModel {
            id: lightningVM
            dayNightRatio: slider.immediateValue
        }
    ]
}
