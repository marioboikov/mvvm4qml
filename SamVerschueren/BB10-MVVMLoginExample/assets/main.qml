import bb.cascades 1.0

Page {
    titleBar: TitleBar {
        title: qsTr("MVVM Login Example")
    }

    Container {
        topPadding: 20.0
        leftPadding: 20.0
        rightPadding: 20.0
        bottomPadding: 20.0

        Label {
            text: qsTr("Welcome, you succesfully logged in!")
        }
    }

    // When the page is created, show the loginsheet
    // This should be edited in the future to see if the user is allready logged in.
    onCreationCompleted: {
        var sheet = loginSheet.createObject();
        sheet.open();
    }

    attachedObjects: [
        // Attach the login sheet to the mainpage.
        ComponentDefinition {
            id: loginSheet
            source: "login.qml"
        }
    ]
}
