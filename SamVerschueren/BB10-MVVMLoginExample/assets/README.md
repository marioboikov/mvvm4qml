# The views

## login.qml
This is the view of the login page. The page shows two inputfields and one
button to the user. The inputfields are bound to the LoginViewModel which
can be found in the /src/ViewModels folder. The button is bound to an action
in the LoginViewModel.

When the button is clicked, the back-end can check the user's credentials
at a webservice. This can be done on a seperate thread. When the server returns
a response, the onLoginCompleted event gets fired and the view is notified. The event
has two parameters. It as a result parameter, which is a boolean. This tells the view
if the user is logged in (true) or the login procedure failed (false). The second parameter
is just a simple string message which gives extra information to what went wrong.

The loginpage is implemented as a sheet which overlays the mainpage of the application.
When the user logs in with the correct credentials, the sheet will be closed and the
application can be used.

## main.qml
This is the view of the home page of the application. It only shows a simple message
indicating that the user logged in correctly. No viewmodel is used for this view.

The main.qml file is loaded at startup of the application. The onCreationCompleted event
is used to show the login page. The login page will then overlay the mainpage.