import bb.cascades 1.0
import bb.system 1.0
import mvvm.viewmodels 1.0

/**
 * This sheet is shown at startup and overlays the application. When the
 * user succesfully logs in, the sheet will be closed.
 */
Sheet {
    id: loginSheet
    
    Page {
        titleBar: TitleBar {
            title: qsTr("MVVM Login Example")
        }
    
        Container {
            topPadding: 50
            leftPadding: 10
            rightPadding: 10
            layout: StackLayout {
            }
    
            // Bind this TextField to the username property of the LoginViewModel.
            TextField {
                id: txtUsername
                text: loginVM.username
                hintText: qsTr("Username")
            }
    
            // Bind this TextField to the password property of the LoginViewModel.
            TextField {
                id: txtPassword
                text: loginVM.password
                hintText: qsTr("Password")
                inputMode: TextFieldInputMode.Password
            }

            // When the button is clicked, invoke the login method of the viewmodel.
            Button {
                text: qsTr("Login")
                horizontalAlignment: HorizontalAlignment.Center
                imageSource: "asset:///images/lock.png"
                onClicked: loginVM.login()
            }
        }
    
        attachedObjects: [
            // Instantiate the LoginViewModel, give it a unique id and bind the properties with the textfields
            
            LoginViewModel {
                id: loginVM
                username: txtUsername.text
                password: txtPassword.text
                // When the ViewModel emits the loginCompleted event, the onLoginCompleted is called
                onLoginCompleted: {
                    /**
                     * The loginCompleted signal has 2 parameters, result and message
                     *      result: true if the user succesfully logged in, false otherwhise
                     *      message: message that says something about the login attempt
                     */
                    if (result) {
                        // close the sheet if the user is succesfully logged in
                        loginSheet.close();
                    } else {
                        // show the errordialog if the user failed to log in
                        errorDialog.body = message;
                        errorDialog.show();
                    }
                }
            },
            SystemDialog {
                id: errorDialog
                cancelButton.label: undefined
                title: qsTr("Error")
                body: qsTr("An unknown error occured")
            }
        ]
    }
}