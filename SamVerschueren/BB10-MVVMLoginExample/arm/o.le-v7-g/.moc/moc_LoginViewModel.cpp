/****************************************************************************
** Meta object code from reading C++ file 'LoginViewModel.hpp'
**
** Created: Mon 25. Feb 21:39:41 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/ViewModels/LoginViewModel.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'LoginViewModel.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LoginViewModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       2,   39, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      25,   16,   15,   15, 0x05,
      59,   50,   15,   15, 0x05,
      99,   84,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
     136,  128,   15,   15, 0x0a,

 // methods: signature, parameters, type, tag, flags
     167,   15,   15,   15, 0x02,

 // properties: name, type, flags
      16,  175, 0x0a495903,
      50,  175, 0x0a495903,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

static const char qt_meta_stringdata_LoginViewModel[] = {
    "LoginViewModel\0\0username\0"
    "usernameChanged(QString)\0password\0"
    "passwordChanged(QString)\0result,message\0"
    "loginCompleted(bool,QString)\0message\0"
    "loginServiceCompleted(QString)\0login()\0"
    "QString\0"
};

void LoginViewModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LoginViewModel *_t = static_cast<LoginViewModel *>(_o);
        switch (_id) {
        case 0: _t->usernameChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->passwordChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->loginCompleted((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 3: _t->loginServiceCompleted((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->login(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData LoginViewModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LoginViewModel::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LoginViewModel,
      qt_meta_data_LoginViewModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LoginViewModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LoginViewModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LoginViewModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LoginViewModel))
        return static_cast<void*>(const_cast< LoginViewModel*>(this));
    return QObject::qt_metacast(_clname);
}

int LoginViewModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getUsername(); break;
        case 1: *reinterpret_cast< QString*>(_v) = getPassword(); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setUsername(*reinterpret_cast< QString*>(_v)); break;
        case 1: setPassword(*reinterpret_cast< QString*>(_v)); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void LoginViewModel::usernameChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void LoginViewModel::passwordChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void LoginViewModel::loginCompleted(bool _t1, const QString & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
