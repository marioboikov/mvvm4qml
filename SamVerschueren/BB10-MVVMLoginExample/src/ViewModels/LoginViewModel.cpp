/*
 * LoginViewModel.cpp
 *
 * This is the implementation of the header file of the LoginViewModel. Look
 * at the header file for more details on this class.
 *
 * Created on: 11-feb.-2013
 * Author: Sam Verschueren        <sam.verschueren@gmail.com>
 */

#include "LoginViewModel.hpp"

/**
 * Creates a new instance of the <em>LoginViewModel</em> and connecting a
 * method to the loginCompleted signal of the <em>LoginService</em>.
 *
 * @param parent The parent object of this class. This used for proper object destruction.
 */
LoginViewModel::LoginViewModel(QObject *parent=0) : QObject(parent) {
    this->loginService = new LoginService(this);

    connect(this->loginService, SIGNAL(loginCompleted(QString)), this, SLOT(loginServiceCompleted(QString)));
}

/**
 * Cleans up the memory by deleting all dynamically created objects.
 */
LoginViewModel::~LoginViewModel() {
    delete loginService;
}

/**
 * Returns the username that is holded by the viewmodel.
 *
 * @return The username
 */
QString LoginViewModel::getUsername() const {
    return this->username;
}

/**
 * Sets the username to the value provided. If the new username is something
 * other then the previous username, the username will be updated and the
 * view will be notified by the usernameChanged signal.
 *
 * @param username The username to be set.
 */
void LoginViewModel::setUsername(const QString &username) {
    if(this->username != username) {
        this->username = username;

        emit usernameChanged(username);
    }
}

/**
 * Returns the password that is holded by the viewmodel.
 *
 * @return The password.
 */
QString LoginViewModel::getPassword() const {
    return this->password;
}

/**
 * Sets the password to the value provided. If the new password is something
 * other then the previous password, the password will be updated and the
 * view will be notified by the passwordChanged signal.
 *
 * @param password The password to be set.
 */
void LoginViewModel::setPassword(const QString &password) {
    if(this->password != password) {
        this->password = password;

        emit passwordChanged(password);
    }
}

/**
 * This method is connected to the loginCompleted signal of the
 * <em>LoginService</em>. When the service is done checking the credentials,
 * this method will be executed.
 *
 * @param message The message that the service returns. If it's empty, login was succesfull.
 */
void LoginViewModel::loginServiceCompleted(const QString &message) {
    bool result = message=="";

    emit loginCompleted(result, message);
}

/**
 * The <em>LoginViewModel</em> does not need to know how the username
 * and password are checked. This is up to the <em>LoginService</em>
 * to do that. The service will get the information from a webservice
 * or just from a file. At this point, I have no idea and I don't need
 * to know it.
 * This method is a Q_INVOKABLE method so it can be bound to the click
 * event of a button.
 */
void LoginViewModel::login() {
    loginService->login(this->username, this->password);
}
