/*
 * LoginService.cpp
 *
 * The implementation of the data service. For more information on this
 * class, please look at the header file.
 *
 * Created on: 25-feb.-2013
 * Author: Sam Verschueren      <sam.verschueren@gmail.com>
 */

#include "LoginService.hpp"

/**
 * Creates a new instance of this data service.
 */
LoginService::LoginService(QObject *parent=0) : QObject(parent) {

}

/**
 * Destroys objects that are dynamically created. For example when we use
 * a <em>NetworkAccessManager</em> we will delete him in this destructor.
 */
LoginService::~LoginService() {

}

/**
 * This method is called in the <em>LoginViewModel</em> and knows how
 * to check the credentials of the user. When the login is completed, the
 * loginCompleted signal is emitted. If the login was succesfull, no message
 * is provided, otherwhise an errormessage will be send with it.
 *
 * @param The username of the login attempt.
 * @param The password of the login attempt.
 */
void LoginService::login(const QString &username, const QString &password) {
    if(username == "Sam" && password == "test") {
        emit loginCompleted("");
    }
    else {
        emit loginCompleted("Please provide the correct username and password.");
    }
}
